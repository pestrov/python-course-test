from django import forms


class ControllerForm(forms.Form):
    bedroom_target_temperature = forms.IntegerField(
        label='Желаемая температура в спальне', min_value=16, max_value=50
    )
    hot_water_target_temperature = forms.IntegerField(
        label='Желаемая температура горячей воды', min_value=24, max_value=90
    )
    bedroom_light = forms.BooleanField(label='Свет в спальне', required=False)
    bathroom_light = forms.BooleanField(label='Свет в ванной', required=False)
