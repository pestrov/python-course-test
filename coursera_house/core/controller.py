import requests
from django.conf import settings


class Controller:
    def __init__(self):
        self.__dict__['auth'] = {'Authorization': 'Bearer {}'.format(settings.SMART_HOME_ACCESS_TOKEN)}

        response = requests.get(settings.SMART_HOME_API_URL, headers=self.auth)
        if response.ok:
            self.__set_data(response)
        else:
            raise requests.RequestException('Не удалось подключиться к серверу')

    def __set_data(self, response):
        response.raise_for_status()
        if response.json().get('status') != 'ok':
            raise IOError(response.json().get('message', ''))

        self.__dict__['old_values'] = {
            controller['name']: controller['value'] for controller in response.json()['data']
        }
        self.__dict__['values'] = self.__dict__['old_values'].copy()

    def get_all(self):
        return self.values

    def set_all(self, values):
        diff = set(values) - set(self.values)
        if diff:
            raise ValueError('Unknown params: %s' % ', '.join(diff))
        self.values.update(values)
        return self.values

    def __getattr__(self, key):
        return self.values.get(key)

    def __setattr__(self, key, value):
        if key in self.__dict__:
            return object.__setattr__(self, key, value)
        if key not in self.values:
            raise AttributeError('Unknown attribute "%s"' % key)
        else:
            self.values[key] = value

    def save(self):
        values_to_send = [
            {'name': name, 'value': value}
            for name, value in self.values.items() if value != self.old_values[name]
        ]
        if values_to_send:
            response = requests.post(
                settings.SMART_HOME_API_URL,
                json={'controllers': values_to_send},
                headers=self.auth
            )
            self.__set_data(response)
            return response.json().get('status') == 'ok'

        return True
