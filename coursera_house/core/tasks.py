from __future__ import absolute_import, unicode_literals
from celery import task

from django.conf import settings
from django.core.mail import send_mail

from .models import Setting
from .controller import Controller


# 1
def check_leak(controller):
    if controller.leak_detector:
        controller.cold_water = False
        controller.hot_water = False
        send_mail('Water is leaking',  # И что будем слать постоянно?
                  'Call the police',
                  'students@coursera.com',
                  [settings.EMAIL_RECEPIENT],
                  fail_silently=True)
        return True
    return False


# 2
def check_cold_water(controller):
    if not controller.cold_water:
        controller.boiler = False
        controller.washing_machine = 'off'
        return False
    return True


# 3
def check_smoke(controller):
    if controller.smoke_detector:
        off = dict.fromkeys([
            'air_conditioner', 'bedroom_light', 'bathroom_light', 'boiler'
        ], False)
        controller.set_all(off)
        controller.washing_machine = 'off'
        return True
    return False


# 4
def check_water_temp(controller):
    boiler_temp = controller.boiler_temperature or 0
    hot_water_target_temperature = Setting.objects.get(controller_name='hot_water_target_temperature').value

    if boiler_temp < hot_water_target_temperature * 0.9:
        controller.boiler = True
    if boiler_temp >= hot_water_target_temperature * 1.1:
        controller.boiler = False


# 5
def check_air_temp(controller):
    bedroom_temp = controller.bedroom_temperature
    bedroom_target_temperature = Setting.objects.get(controller_name='bedroom_target_temperature').value

    if bedroom_temp > bedroom_target_temperature * 1.1:
        controller.air_conditioner = True

    if bedroom_temp < bedroom_target_temperature * 0.9:
        controller.air_conditioner = False


# 6
def check_curtains(controller):
    if controller.outdoor_light < 50 and not controller.bedroom_light:
        controller.curtains = 'open'
    elif controller.outdoor_light > 50 or controller.bedroom_light:
        controller.curtains = 'close'


@task()
def smart_home_manager():
    controller = Controller()

    leak = check_leak(controller)
    smoke = check_smoke(controller)
    cold_water = check_cold_water(controller)
    if cold_water and not (leak or smoke):
        check_water_temp(controller)

    if not smoke:
        check_air_temp(controller)
    if controller.curtains != 'slightly_open':
        check_curtains(controller)
    return controller.save()
