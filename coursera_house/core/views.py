from django.urls import reverse_lazy
from django.views.generic import FormView
from django.http import HttpResponseServerError


from .models import Setting
from .controller import Controller
from .form import ControllerForm
import requests
import json


class ControllerView(FormView):
    form_class = ControllerForm
    template_name = 'core/control.html'
    controller = None
    success_url = reverse_lazy('form')

    def dispatch(self, request, *args, **kwargs):
        try:
            self.controller = Controller()
        except Exception as e:
            return HttpResponseServerError('Не удалось получить данные с сервера {}'.format(e), status=502)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ControllerView, self).get_context_data()
        context['data'] = self.controller.get_all()
        return context

    def get_initial(self):
        initial = {
            'bedroom_light':self.controller.bedroom_light,
            'bathroom_light': self.controller.bathroom_light
        }
        initial.update({
            item.controller_name: item.value for item in Setting.objects.all()
        })
        return initial

    def form_valid(self, form):
        self.controller.bedroom_light = form.cleaned_data['bedroom_light']
        self.controller.bathroom_light = form.cleaned_data['bathroom_light']
        self.controller.save()

        for item in Setting.objects.all():
            item.value = form.cleaned_data[item.controller_name]
            item.save()
        return super(ControllerView, self).form_valid(form)
