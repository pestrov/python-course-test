import json

import pytest
import responses
from django.conf import settings
from django.core import mail

from coursera_house.core.tasks import smart_home_manager


class TestSmartHomeManager():
    """smart_home_manager"""

    @responses.activate
    def test_leak(self, client, db, response_ok, record_xml_attribute):
        """smart_home_manager task sends request to close cold and hot water valves and disable washing machine and boiler if leak is detected."""
        record_xml_attribute('description', self.test_leak.__doc__)
        controller_url = settings.SMART_HOME_API_URL
        headers = {'Authorization': 'Bearer {}'.format(settings.SMART_HOME_ACCESS_TOKEN)}
        for sensor in response_ok['data']:
            if sensor['name'] == 'leak_detector':
                sensor['value'] = True
        responses.add(responses.GET, controller_url, json=response_ok, status=200, headers=headers)
        responses.add(responses.POST, controller_url, json=response_ok, status=200, headers=headers)
        smart_home_manager()
        # Серверу должен быть отправлен один GET запрос для получения данных и
        # POST запрос с новыми установками.
        assert len(responses.calls) == 2
        call = responses.calls[1].request
        assert call.method == 'POST'
        document = json.loads(call.body)
        expected = [{
            'name': 'cold_water', 'value': False
        }, {
            'name': 'hot_water', 'value': False,
        }, {
            'name': 'boiler', 'value': False,
        }, {
            'name': 'washing_machine', 'value': 'off',
        }]
        for command in expected:
            assert command in document['controllers']
        # Отправляется письмо на email указанный в настройках
        assert len(mail.outbox) == 1
        assert settings.EMAIL_RECEPIENT in mail.outbox[0].to

    @responses.activate
    @pytest.mark.parametrize('input_parameters', [
        # не отправляет запрос на включение бойлера, если холодная вода
        # отключена, даже если вода в бойлере холодная.
        {'boiler_temperature': 71, 'boiler': False, 'cold_water': False, 'washing_machine': 'off'},
        # не отправляет запрос открытие/закрытие штор, если они в ручном режиме.
        {'curtains': 'slightly_open', 'outdoor_light': 49, 'bedroom_light': False},
        {'curtains': 'slightly_open', 'outdoor_light': 51, 'bedroom_light': False},
        {'curtains': 'slightly_open', 'outdoor_light': 49, 'bedroom_light': True},
        # не отправляет запрос на включение бойлера, если обнаружен дым,
        # даже если вода в бойлере холодная.
        {'boiler_temperature': 71, 'boiler': False, 'smoke_detector': True,
         'air_conditioner': False, 'bedroom_light': False, 'washing_machine': 'off'},
        # Не отправляет запрос на включение кондиционера, если в комнате есть
        # дым, даже если в комнате слишком жарко.
        {'air_conditioner': False, 'bedroom_temperature': 24, 'smoke_detector': True,
         'bedroom_light': False, 'boiler': False, 'washing_machine': 'off'},

    ])
    def test_no_reactions(self, client, db, response_ok, input_parameters, record_xml_attribute):
        """smart_home_manager task does not send request in certian conditions."""
        record_xml_attribute('description', self.test_no_reactions.__doc__)
        controller_url = settings.SMART_HOME_API_URL
        headers = {'Authorization': 'Bearer {}'.format(settings.SMART_HOME_ACCESS_TOKEN)}
        for sensor in response_ok['data']:
            for key, value in input_parameters.items():
                if sensor['name'] == key:
                    sensor['value'] = value
        responses.add(responses.GET, controller_url, json=response_ok, status=200, headers=headers)
        responses.add(responses.POST, controller_url, json=response_ok, status=200, headers=headers)
        smart_home_manager()
        # Серверу должен быть отправлен один GET запрос для получения данных.
        assert len(responses.calls) == 1
        call = responses.calls[0].request
        assert call.method == 'GET'

    @pytest.mark.parametrize('input_parameters, output_parameters', [
        # отправляет запрос на отключение стиральной машины и бойлера, если
        # отключена холодная вода.
        ({'cold_water': False, 'boiler': True, 'washing_machine': 'on'},
         {'boiler': False, 'washing_machine': 'off'}),
        # отправляет запрос на включение бойлера, если вода слишком холодная.
        ({'boiler': False, 'boiler_temperature': 71}, {'boiler': True}),
        # отправляет запрос на отключение бойлера, если вода слишком горячая.
        ({'boiler': True, 'boiler_temperature': 89}, {'boiler': False}),
        # отправляет запрос на открытие штор, если на улице темно и не включен
        # свет.
        ({'curtains': 'close', 'outdoor_light': 49, 'bedroom_light': False}, {'curtains': 'open'}),
        # отправляет запрос на закрытие штор, если на улице светло.
        ({'curtains': 'open', 'outdoor_light': 51, 'bedroom_light': False}, {'curtains': 'close'}),
        # отправляет запрос на закрытие штор, если включен свет.
        ({'curtains': 'open', 'outdoor_light': 49, 'bedroom_light': True}, {'curtains': 'close'}),
        # отправляет запрос на отключение всех электроприборов если обнаружен дым.
        ({'smoke_detector': True, 'air_conditioner': True, 'bedroom_light': True,
          'bathroom_light': True, 'boiler': True, 'washing_machine': 'on'},
         {'air_conditioner': False, 'bedroom_light': False, 'bathroom_light': False,
          'boiler': False, 'washing_machine': 'off'}),
        # отправляет запрос на включение кондиционера, если в комнате слишком жарко.
        ({'air_conditioner': False, 'bedroom_temperature': 24}, {'air_conditioner': True}),
        # отправляет запрос на отключение кондиционера, если в комнате слишком холодно.
        ({'air_conditioner': True, 'bedroom_temperature': 18}, {'air_conditioner': False}),
    ])
    @responses.activate
    def test_reactions(self, client, db, response_ok, input_parameters, output_parameters, record_xml_attribute):
        """smart_home_manager task does send request in certian conditions."""
        record_xml_attribute('description', self.test_reactions.__doc__)
        controller_url = settings.SMART_HOME_API_URL
        headers = {'Authorization': 'Bearer {}'.format(settings.SMART_HOME_ACCESS_TOKEN)}
        for sensor in response_ok['data']:
            for key, value in input_parameters.items():
                if sensor['name'] == key:
                    sensor['value'] = value
        responses.add(responses.GET, controller_url, json=response_ok, status=200, headers=headers)
        responses.add(responses.POST, controller_url, json=response_ok, status=200, headers=headers)
        smart_home_manager()
        # Серверу должен быть отправлен один GET запрос для получения данных и
        # POST запрос с новыми установками.
        assert len(responses.calls) == 2
        call = responses.calls[1].request
        assert call.method == 'POST'
        document = json.loads(call.body)
        print(document)
        for key, value in output_parameters.items():
            assert {'name': key, 'value': value} in document['controllers']
