import os.path
import pytest
import json


RESPONSES_DIR = os.path.join(os.path.dirname(__file__), 'responses')


@pytest.fixture()
def response_ok():
    """Фикстура с ответом от сервера с контроллером."""
    path = os.path.join(RESPONSES_DIR, 'ok.json')
    with open(path) as fp:
        return json.load(fp)
