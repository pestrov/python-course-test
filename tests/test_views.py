import json

import pytest
import responses
from django.conf import settings

from coursera_house.core.models import Setting


class TestViews():

    @responses.activate
    def test_get_controller_page(self, client, db, response_ok, record_xml_attribute):
        """/ (GET) returns html page with sensors data."""
        record_xml_attribute('description', self.test_get_controller_page.__doc__)
        controller_url = settings.SMART_HOME_API_URL
        headers = {'Authorization': 'Bearer {}'.format(settings.SMART_HOME_ACCESS_TOKEN)}
        responses.add(responses.GET, controller_url,
                      json=response_ok, status=200, headers=headers)

        response = client.get('/')
        assert response.status_code == 200
        assert response['Content-Type'] == 'text/html; charset=utf-8'
        document = response.content.decode('utf-8')

        for sensor in response_ok['data']:
            # TODO: Проверять значения после того как будет улучшена верстка
            # шаблона
            assert sensor['name'] in document

        # TODO: Проверять детали формы после того как будет использоваться Form
        assert '</form>' in document

    @responses.activate
    def test_get_controller_page_fail(self, client, db, record_xml_attribute):
        """/ (GET) returns 502 with error message if smart home API is unavailable."""
        record_xml_attribute('description', self.test_get_controller_page_fail.__doc__)
        controller_url = settings.SMART_HOME_API_URL
        headers = {'Authorization': 'Bearer {}'.format(settings.SMART_HOME_ACCESS_TOKEN)}
        responses.add(responses.GET, controller_url, status=500, headers=headers)

        response = client.get('/')
        assert response.status_code == 502
        assert response['Content-Type'] == 'text/html; charset=utf-8'
        response.content.decode('utf-8')

    @responses.activate
    def test_post_settings(self, client, db, response_ok, record_xml_attribute):
        """/(POST) sends data to smart home API if data is correct."""
        record_xml_attribute('description', self.test_post_settings.__doc__)
        controller_url = settings.SMART_HOME_API_URL
        headers = {'Authorization': 'Bearer {}'.format(settings.SMART_HOME_ACCESS_TOKEN)}
        responses.add(responses.GET, controller_url, json=response_ok, status=200, headers=headers)
        responses.add(responses.POST, controller_url, json=response_ok, status=200, headers=headers)

        data = {
            'bedroom_target_temperature': 20,
            'hot_water_target_temperature': 50,
            'bedroom_light': False,
            'bathroom_light': True
        }
        client.post('/', data=data)
        # Серверу должен быть отправлен один GET запрос на получение данных и
        # один POST запрос с управляемыми напрямую установками.
        assert len(responses.calls) == 2
        call = responses.calls[1].request
        assert call.method == 'POST'
        document = json.loads(call.body)
        expected = {
            'controllers': [{
                'name': 'bedroom_light', 'value': False
            }, {
                'name': 'bathroom_light', 'value': True,
            }]
        }
        assert document == expected
        # Установки рекомендуемой температуры должны быть записаны в БД
        bedroom_temp = Setting.objects.get(controller_name='bedroom_target_temperature')
        assert bedroom_temp.value == 20
        water_temp = Setting.objects.get(controller_name='hot_water_target_temperature')
        assert water_temp.value == 50

    @pytest.mark.parametrize('field, value', [
        ('bedroom_target_temperature', -100),
        ('bedroom_target_temperature', 100),
        ('bedroom_target_temperature', 'hundred'),
    ])
    def test_post_settings_fail(self, client, db, response_ok, field, value, record_xml_attribute):
        """/ (POST) does not sends data to smart home API or saves to DB if data is incorrect."""
        record_xml_attribute('description', self.test_post_settings_fail.__doc__)
        controller_url = settings.SMART_HOME_API_URL
        headers = {'Authorization': 'Bearer {}'.format(settings.SMART_HOME_ACCESS_TOKEN)}
        responses.add(responses.GET, controller_url, json=response_ok, status=200, headers=headers)
        responses.add(responses.POST, controller_url, json=response_ok, status=200, headers=headers)

        data = {
            'bedroom_target_temperature': 20,
            'hot_water_target_temperature': 50,
            'bedroom_light': True,
            'bathroom_light': False
        }
        data[field] = value
        response = client.post('/', data=data)
        # Серверу не передавались данные
        assert len(responses.calls) == 0
        # Установки рекомендуемой температуры должны остаться без изменений
        bedroom_temp = Setting.objects.get(controller_name='bedroom_target_temperature')
        assert bedroom_temp.value == 21
        water_temp = Setting.objects.get(controller_name='hot_water_target_temperature')
        assert water_temp.value == 80
